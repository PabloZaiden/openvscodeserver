ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=America/New_York
ARG OPENVSCODE_RELEASE_BINARY_URL=https://github.com/gitpod-io/openvscode-server/releases/download/openvscode-server-v1.67.2/openvscode-server-v1.67.2-linux-x64.tar.gz
ARG PACKAGES_MICROSOFT_PROD_URL=https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb

FROM ubuntu:latest

ARG DEBIAN_FRONTEND
ARG TZ
ARG OPENVSCODE_RELEASE_BINARY_URL
ARG PACKAGES_MICROSOFT_PROD_URL

RUN apt update
RUN apt install -y curl sudo git nano bash wget zip python

RUN addgroup --gid ${GID:-1000} docker
RUN adduser --uid ${UID:-1000} --gid ${GID:-1000} --disabled-password --gecos '' docker 
RUN adduser docker sudo 
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers  

WORKDIR /home/docker

# dotnet
ADD $PACKAGES_MICROSOFT_PROD_URL /home/docker/packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN rm packages-microsoft-prod.deb

RUN apt update; \
  apt install -y apt-transport-https && \
  apt update && \
  apt install -y dotnet-sdk-6.0


# node 
RUN curl -fsSL https://deb.nodesource.com/setup_current.x | bash -
RUN apt install -y nodejs

USER docker

ADD $OPENVSCODE_RELEASE_BINARY_URL /home/docker/openvscode.tar.gz
RUN sudo chown docker:docker ./openvscode.tar.gz
RUN tar -xzf openvscode.tar.gz
RUN mv openvscode-* openvscode
ENTRYPOINT /home/docker/openvscode/bin/openvscode-server --without-connection-token --host 0.0.0.0
